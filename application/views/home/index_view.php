<?php $this->load->view('templates/header_view', $company[0]); ?>        
<header>
    <div class="container">
        <div class="intro-text">
            <h1>Welcome To SOCIANOVATION!</h1>
<h2>It's Nice To Meet You</h2>
                            <a class="page-scroll btn btn-xl" href="#about">Tell me more</a>
                    </div>
    </div>
</header>
<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Services</h2>
                <h3>We plan, build, and deliver it for you.</h3>
            </div>
        </div>
        <div class="row text-center">
            <?php foreach($services as $service) { ?>
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa <?php echo $service['image']; ?> fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading"><?php echo $service['title']; ?></h4>
                <p class="text-muted"><?php echo $service['description']; ?></p>
            </div>
            <?php } ?>
        </div>
    </div>
</section>

             <section id="portfolio" class="bg-light-gray">
  <div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2>Portfolio</h2>
<h3>Our Works so far.</h3>
        </div>
    </div>
    <div class="row">

        <?php foreach($portfolios as $portfolio){ ?>
        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal<?php echo $portfolio['id']; ?>" class="portfolio-link" data-toggle="modal">
            <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                    <i class="fa fa-plus fa-3x"></i>
                </div>
            </div>
                <img src="<?php echo base_url(); ?>assets/images/portfolio/<?php echo $portfolio['thumbnail']; ?>" class="img-responsive" alt="">
            </a>
            <div class="portfolio-caption">
                <h4><?php echo $portfolio['name']; ?></h4>
                <p class="text-muted"><a href="<?php echo $portfolio['link']; ?>" target="_blank"><?php echo $portfolio['link']; ?></a></p>
            </div>
        </div>
        <?php } ?>

    </div>
</div>
</section>

    <?php foreach($portfolios as $portfolio) {?>
     <div class="portfolio-modal modal fade" id="portfolioModal<?php echo $portfolio['id']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2><?php echo $portfolio['name']; ?></h2>
                            <hr class="star-primary">
                            <img src="<?php echo base_url(); ?>assets/images/portfolio/<?php echo $portfolio['image']; ?>" class="img-responsive img-centered" alt="image-alt">
                            <p><?php echo $portfolio['description']; ?></p>
                            <ul class="list-inline item-details">
                                <li>Client:
                                    <strong><a href="#"><?php echo $portfolio['client']; ?></a>
                                    </strong>
                                </li>
                                <li>Date:
                                    <strong><a href="#"><?php echo date("F y", strtotime($portfolio['date'])); ?></a>
                                    </strong>
                                </li>
                                <li>Service:
                                    <strong><a href="#"><?php echo $portfolio['service']; ?></a>
                                    </strong>
                                </li>
                            </ul>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>

<section id="about">
  <div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2>About</h2>
            <h3>Our journey was just started and it still continues.</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <ul class="timeline">
                <?php foreach($histories as $history){ ?>
                <li <?php echo (strtolower($history['type']) == 'right') ? 'class="timeline-inverted"' : '';  ?>>
                    <div class="timeline-image">
                        <img class="img-circle img-responsive" src="<?php echo base_url(); ?>assets/images/history/<?php echo $history['image']; ?>" alt="">
                    </div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4><?php echo $history['date_description']; ?></h4>
                            <h4 class="subheading"><?php echo $history['title']; ?></h4>
                        </div>
                        <div class="timeline-body">
                            <p class="text-muted"><?php echo $history['description']; ?></p>
                        </div>
                    </div>
                </li>
                <?php } ?>
                <li class="timeline-inverted">
                    <div class="timeline-image">
                        <h4>Be Part<br />Of Our <br />Story!</h4>
                    </div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4></h4>
                            <h4 class="subheading"></h4>
                        </div>
                        <div class="timeline-body">
                            <p class="text-muted"></p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>   
    </div>
</div>
</section>
            <section id="team" class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Our Amazing Team</h2>
                <h3>Yes, we're still a small team, but we never stop growing.</h3>
            </div>
        </div>

        <div class="row">
        <?php foreach($teams as $team) {?>
            <div class="col-sm-6">
                <div class="team-member">
                    <img src="<?php echo base_url(); ?>assets/images/team/<?php echo $team['photo']; ?>" class="img-responsive img-circle" alt="">
                    <h4><?php echo $team['name']; ?></h4>
                    <p class="text-muted"><?php echo $team['structure_title']; ?> - <?php echo $team['job_title']; ?></p>
                    <ul class="list-inline social-buttons">
                        <li><a href="<?php echo $team['twitter']; ?>"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="<?php echo $team['facebook']; ?>"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="<?php echo $team['linkedin']; ?>"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        <?php } ?>
        </div>

        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <p class="large text-muted">We work together, sharing our goal together, and help each other to achive it.</p>
            </div>
        </div>
    </div>
</section>
            <section id="contact">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
       <h2>CONTACT US</h2>
        <h3>Feel free to contact us by filling these form below, we will contact you back as soon as we received your submission.</h3>
       
     </div>
   </div>
   <div class="row">
        <form name="simple_form" id="simple_form" action="https://getsimpleform.com/messages?form_api_token=b673e9c9cae1c1a549edb801b2b0a13e" enctype="multipart/form-data" method="post">

        <input type="hidden" name="redirect_to" value="<?php echo site_url('thankyou'); ?>" />

        <div class="col-md-6">
        <div class="form-group">
            <input type="text" name="name" id="name" placeholder="Name" required class="form-control" />
        </div>
        <div class="form-group">
            <input type="email" name="email" id="email" placeholder="name@example.com" required class="form-control" />
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <textarea name="message" rows="8" id="message" placeholder="Your message here" required class="form-control"></textarea>
                    </div>
    </div>
    <div class="col-lg-12 text-center">
        <div class="form-group">
            <button class="btn btn-primary btn-lg">Submit</button>
        </div>
    </div>
</form>

        </div>
  </div>
</section>

      



<?php $this->load->view('templates/footer_view', $company[0]); ?>