<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
  <title>Home | SOCIANOVATION - Be Social, Be Innovative</title>
<meta name="description" content="SOCIANOVATION is a group of developers and designers who mastered the art of web technologies to provide clients a new way to publish their business." />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/agency/img/favicon.png" />

                  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Orbitron:900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url(); ?>assets/agency/css-compiled/global.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/agency/css/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/plugins/login/css/login.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/custom.css" type="text/css" rel="stylesheet" />


  </head>
<body id="page-top" class="index">

      <!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" title="SOCIANOVATION" href="#page-top">SOCIA<span style="color:#FFF;">NOVATION</a></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li><a class="page-scroll" href="#services" >Services</a></li>
                <li><a class="page-scroll" href="#portfolio" >Portfolio</a></li>
                <li><a class="page-scroll" href="#about" >About</a></li>
                <li><a class="page-scroll" href="#team" >Team</a></li>
                <li><a class="page-scroll" href="#contact" >Contact</a></li>
                                                    </ul>
        </div>
       <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>    
