<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <span class="copyright">Copyright &copy; <a href="https://www.socianovation.com">SOCIANOVATION</a> <?php echo $year;?></span>
            </div>
            <div class="col-md-4">
                <ul class="list-inline social-buttons">
                                            <li><a href="<?php echo $twitter_link; ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                        </li>
                                            <li><a href="<?php echo $facebook_link; ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                        </li>
                                            <li><a href="<?php echo $github_link; ?>" target="_blank"><i class="fa fa-github"></i></a>
                        </li>
                                    </ul>
            </div>
            <div class="col-md-4">
                <ul class="list-inline quicklinks">
                                            <li><a class="page-scroll" href="#about">About Us</a>
                        </li>
                                            <li><a class="page-scroll" href="#contact">Contact Us</a>
                        </li>
                                    </ul>
            </div>
        </div>
    </div>
</footer>  
                              <script src="<?php echo base_url(); ?>assets/jquery/jquery-2.x.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url(); ?>assets/agency/js/bootstrap.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url(); ?>assets/agency/js/jquery.easing.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url(); ?>assets/agency/js/classie.js" type="text/javascript" ></script>
<script src="<?php echo base_url(); ?>assets/agency/js/cbpAnimatedHeader.js" type="text/javascript" ></script>
<script src="<?php echo base_url(); ?>assets/agency/js/agency.js" type="text/javascript" ></script>


</body>
</html>
