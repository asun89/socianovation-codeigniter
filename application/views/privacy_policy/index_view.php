<?php $this->load->view('templates/header_insight_view'); ?>
<div class="insight-detail-header">
    <div class="title"><h1>AMCORE Privacy Policy</h1></div>
    <div class="long-content">
        <p><span style="font-weight: 400;">Thank you for downloading our game! This Privacy Policy describes:</span></p>
        <ul>
        <li style="font-weight: 400;"><span style="font-weight: 400;">How we collect personal data about you and why we do so </span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">How we use your personal data, and</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">The choices you have about your personal data.</span></li>
        </ul>
        <p>&nbsp;</p>
        <p><span style="font-weight: 400;">This Privacy Policy applies to Amcore&rsquo;s games, websites and related services, which we here collectively call the Service. We may periodically update this Privacy Policy by posting a new version on amcodesign.com. If we make any material changes, we will notify you by posting a notice in the Service prior to the change becoming effective. Your continued use of the Service after the effective date will be subject to the new Privacy Policy.</span></p>
        <p>&nbsp;</p>
        <p><span style="font-weight: 400;">If you have questions about the data protection, or if you have any requests for resolving issues with your personal data, we encourage you to primarily contact us through email address </span><a href="mailto:amcore@amcodesign.com"><span style="font-weight: 400;">amcore@amcodesign.com</span></a><span style="font-weight: 400;"> so we can reply to you more quickly.</span></p>
        <p>&nbsp;</p>
        <p><strong>The data we may collect</strong></p>
        <ul>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Contact information (such as name and email address)</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Profile information (such as profile photo)</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Your messages to the Service (such as chat logs and player support tickets)</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Other data you choose to give us (such as data to identify a lost account)</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Data about your account and game progress</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Your IP address and mobile device identifiers (such as your device ID, advertising ID, MAC address, IMEI)</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Data about your device, such as device name and operating system, browser type and language</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Data we collect with cookies and similar technologies (see more below)</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">General location data</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Precise geo-location data (GPS, with your consent)</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Data about your use of the Service, such as gameplay data and your interactions with other players inside the Service.</span></li>
        </ul>
        <p>&nbsp;</p>
        <p><strong>Data we collect from our partners</strong></p>
        <ul>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Data we receive if you link a third party tool with the Service (such as Facebook or Google)</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Demographic data (such as to determine the coarse location of your IP address)</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Data to fight fraud (such as refund abuse in games or click fraud in advertising)</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Data from platforms that the games run on (such as to verify payment)</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Data for advertising and analytics purposes, so we can provide you a better Service</span></li>
        </ul>
        <p>&nbsp;</p>
        <p><strong>Why do we collect your data</strong></p>
        <ul>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Create accounts and allow you to play our games and use our Service</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Operate the Service</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Verify and confirm payments</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Provide and deliver products and services you request</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Send you Service-related communications</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Provide social features as part of the Service</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Customize your Service experience</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Respond to your comments and questions and provide player support</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Provide you Amcore offers in the Service as well as in other websites and services, and by email</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Send you related information, such as updates, security alerts, and support messages</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Enable you to communicate with other players</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">To show personalized advertisements.</span></li>
        </ul>
        <p><span style="font-weight: 400;">To show you personalized advertisements in the Service as well as in other websites and services (including email) we have a legitimate interest to process necessary data to</span></p>
        <ul>
        <ul>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Track the content you access in connection with the Service and your online behavior</span></li>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Deliver, target and improve our advertising and the Service</span></li>
        </ul>
        </ul>
        <p><span style="font-weight: 400;">For information on how to opt-out from personalized advertisements, see section 'Your rights and options' below.</span></p>
        <ul>
        <li style="font-weight: 400;"><span style="font-weight: 400;">To analyze, profile, and segment.</span></li>
        </ul>
        <p><span style="font-weight: 400;">In all of the above cases and purposes, we may analyze, profile and segment all collected data.</span></p>
        <ul>
        <li style="font-weight: 400;"><span style="font-weight: 400;">With your consent.</span></li>
        </ul>
        <p><span style="font-weight: 400;">With your consent, we may process your data for additional purposes, such as using your GPS location to show you local events.</span></p>
        <p>&nbsp;</p>
        <p><strong>Who can see your data</strong></p>
        <p><span style="font-weight: 400;">Apart from Amcore, your data can be accessed by others in the following situations:</span></p>
        <ul>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Other player and user</span></li>
        </ul>
        <p><span style="font-weight: 400;">Social features are a core component of our games. Other players and users may, for example, see your profile data, in-game activities and read the messages you have posted.</span></p>
        <ul>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Partners working for Amcore</span></li>
        </ul>
        <p><span style="font-weight: 400;">Amcore has partners to perform services for us. These partners process your data only at and according to Amcore&rsquo;s instructions to provide the Service, such as hosting, player support, advertising, analytics and fraud prevention.</span></p>
        <ul>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Other companies and public authorities.</span></li>
        </ul>
        <p><span style="font-weight: 400;">In order to combat fraud and illegal activity, we may exchange data with other companies and organizations and provide it to public authorities in response to lawful requests.</span></p>
        <p>&nbsp;</p>
        <p><span style="font-weight: 400;">We may also disclose your data based on your consent, to comply with the law or to protect the rights, property or safety of us, our players or others.</span></p>
        <ul>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Advertising and Social Media partners.</span></li>
        </ul>
        <p><span style="font-weight: 400;">The Service includes features from our partners, such as social media interaction tools and in-game advertising. These partners may access your data and operate under their own privacy policies. We encourage you to check their privacy policies to learn more about their data processing practices.</span></p>
        <ul>
        <li style="font-weight: 400;"><span style="font-weight: 400;">International data transfers</span></li>
        </ul>
        <p><span style="font-weight: 400;">Our Service is global by nature and your data can therefore be transferred to anywhere in the world. Because different countries may have different data protection laws than your own country, we take steps to ensure adequate safeguards are in place to protect your data as explained in this Policy. Adequate safeguards that our partners may use include standard contractual clauses approved by EU Commission and the Privacy Shield certification in case of transfers to the USA.</span></p>
        <p>&nbsp;</p>
        <p><strong>You right and options</strong></p>
        <ul>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Opt-out of marketing emails and other direct marketing.</span></li>
        </ul>
        <p><span style="font-weight: 400;">You may opt-out of receiving promotional communications, such as marketing emails from us by following the instructions in such communications.</span></p>
        <ul>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Opt-out of targeted advertising.</span></li>
        </ul>
        <p><span style="font-weight: 400;">You can opt-out of interest-based advertising on mobile applications by checking the privacy settings of your Android or iOS device and selecting "limit ad tracking" (Apple iOS) or "opt-out of interest based ads" (Android).</span></p>
        <p><span style="font-weight: 400;">For mobile advertising in apps, you can reset your Advertising Identifier and depending on your device, select to opt out of interest-based ads (Android) or turn on the Limit Ad Tracking setting (iOS).</span></p>
        <p><span style="font-weight: 400;">Amcore also works with a number of other analytics and ad serving partners within our mobile apps and websites. These partners can use cookies, web beacons, and other tracking technologies to collect or receive data about you and might claim controller rights over your personal data. For more information about their privacy practices and opt-out possibilities, please visit the links below.</span></p>
        <p>&nbsp;</p>
        <p><strong>Facebook</strong></p>
        <p><span style="font-weight: 400;">Privacy Policy: https://www.facebook.com/about/privacy</span></p>
        <p>&nbsp;</p>
        <p><strong>Google</strong></p>
        <p><span style="font-weight: 400;">Privacy Policy: https://policies.google.com/privacy</span></p>
        <p>&nbsp;</p>
        <p><strong>Twitter</strong></p>
        <p><span style="font-weight: 400;">Privacy Policy: https://twitter.com/en/privacy</span></p>
        <p>&nbsp;</p>
        <p><strong>Unity Technologies</strong></p>
        <p><span style="font-weight: 400;">Privacy Policy: </span><a href="https://unity3d.com/legal/privacy-policy"><span style="font-weight: 400;">https://unity3d.com/legal/privacy-policy</span></a></p>
        <p>&nbsp;</p>
        <ul>
        <li style="font-weight: 400;"><span style="font-weight: 400;">For personalized in-game offers opt-out, you can use the options provided in the game settings.</span></li>
        </ul>
        <ul>
        <li style="font-weight: 400;"><span style="font-weight: 400;">Access the personal data we hold about you.</span></li>
        </ul>
        <p><span style="font-weight: 400;">If you request, we will provide you a copy of your personal data in an electronic format.</span></p>
        <ul>
        <li style="font-weight: 400;"><span style="font-weight: 400;">You also have the right to correct your data, have your data deleted, object how we use or share your data, and restrict how we use or share your data. You can always withdraw your consent, for example by turning off GPS location sharing in your mobile device settings.</span></li>
        </ul>
        <p>&nbsp;</p>
        <p><strong>How do we protect your data</strong></p>
        <p><span style="font-weight: 400;">The security of your personal information is important to us. We follow generally accepted industry standards to protect your information, both during transmission and once it is received. However, no method of transmission over the Internet, or method of electronic storage, is 100% secure. Therefore, we cannot guarantee its absolute security. While no security system is completely secure, we and our Service Provider stake appropriate security measures to protect against unauthorized access or disclosure of the information collected by us.</span></p>
    </div>
</div>
<?php $this->load->view('templates/footer_view'); ?>