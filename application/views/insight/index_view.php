<?php $this->load->view('templates/header_insight_view'); ?>

<div class="insight-header">
    <img src="<?php echo base_url("assets/img/insight/header.jpg"); ?>?rand=<?php echo $this->config->item('resource_version'); ?>" />
</div>
<div class="insight-content">
    <div class="insight-featured">
        <?php if($insight_featured_top) { ?>
        <div class="left">
            <a class="not_visible_link"  href="<?php echo site_url("insight/detail/".$insight_featured_top['slug']); ?>">
                <div class="image-featured">
                    <img src="<?php echo base_url("assets/img/insight_post/".$insight_featured_top['thumbnail_featured']); ?>?rand=<?php echo $this->config->item('resource_version'); ?>" />
                </div>
                <div class="description">
                    <div class="category"><?php echo $insight_featured_top['category_name']; ?></div>
                    <div class="title"><?php echo $insight_featured_top['title']; ?></div>
                </div>
             </a>
        </div>
        <?php } ?>
        <?php if($insight_featured_left || $insight_featured_right) { ?>
        <div class="right">
            <?php if($insight_featured_left) { ?>
            <a class="not_visible_link"  href="<?php echo site_url("insight/detail/".$insight_featured_left['slug']); ?>">
            <div class="insight-featured-mini">
                <div class="featured-image-mini">
                    <img src="<?php echo base_url("assets/img/insight_post/".$insight_featured_left['thumbnail_featured_mini']); ?>?rand=<?php echo $this->config->item('resource_version'); ?>">
                </div>
                <div class="featured-content-mini">
                    <div class="category"><?php echo $insight_featured_left['category_name']; ?></div>
                    <div class="title"><?php echo $insight_featured_left['title']; ?></div>
                </div>
            </div>
            </a>
            <?php } ?>
            <?php if($insight_featured_right) { ?>
            <a class="not_visible_link"  href="<?php echo site_url("insight/detail/".$insight_featured_right['slug']); ?>">
            <div class="insight-featured-mini">
                <div class="featured-image-mini">
                    <img src="<?php echo base_url("assets/img/insight_post/".$insight_featured_right['thumbnail_featured_mini']); ?>?rand=<?php echo $this->config->item('resource_version'); ?>">
                </div>
                <div class="featured-content-mini">
                    <div class="category"><?php echo $insight_featured_right['category_name']; ?></div>
                    <div class="title"><?php echo $insight_featured_right['title']; ?></div>
                </div>
            </div>
            </a>
            <?php } ?>
        </div>
        <?php } ?>
    </div>

    <div class="insight-search-field">
        <input type="text" placeholder="Search Our Insight" name="search" id="search" />
    </div>

    <div class="insight-posts">
        <?php foreach($insights_not_featured as $insight){ ?>
        <a class="not_visible_link"  href="<?php echo site_url("insight/detail/".$insight['slug']); ?>">
        <div class="insight-post-container">
            <div class="insight-post">
                <div class="insight-post-image">
                    <img src="<?php echo base_url("assets/img/insight_post/".$insight['thumbnail']); ?>?rand=<?php echo $this->config->item('resource_version'); ?>" />
                </div>
                <div class="insight-post-content">
                    <div class="category"><?php echo $insight['category_name']; ?></div>
                    <div class="title"><?php echo $insight['title']; ?></div>
                    <div class="short-content"><?php echo $insight['short_content']; ?></div>
                </div>  
            </div>
        </div>
        </a>
        <?php } ?>
    </div>

    <div class="seemore-section">
        <a href="#" id="seemore">See More</a>
    </div>

</div>
<div class="insight-footer">
    <div class="facebook-content">
        <div class="facebook-container">
        <div class="facebook-header">Facebook</div>
        <div class="fb-page" data-href="https://www.facebook.com/AmcoGraphicDesign/" 
            data-tabs="timeline" 
            data-small-header="false" 
            data-adapt-container-width="true" 
            data-hide-cover="false" 
            data-show-facepile="true">
            <blockquote cite="https://www.facebook.com/AmcoGraphicDesign/" 
            class="fb-xfbml-parse-ignore">
            <a href="https://www.facebook.com/AmcoGraphicDesign/">Amco ID</a>
        </blockquote></div>
        </div>
    </div>
    <div class="instagram-content">
        <div class="instagram-container">
        <div class="instagram-header">Instagram</div>
            <!-- InstaWidget -->
            <a href="https://instawidget.net/v/user/amcocreative" id="link-ed8998932e94d6d781721a17504608612bd490ab85e0ba1f8ab8b35c9f15376a">@amcocreative</a>
            <script src="https://instawidget.net/js/instawidget.js?u=ed8998932e94d6d781721a17504608612bd490ab85e0ba1f8ab8b35c9f15376a&width=340px"></script>
        </div>
    </div>
</div>

<a class="not_visible_link hidden" id="hidden_post"  href="<?php echo site_url("insight/detail/".$insight['slug']); ?>">
    <div class="insight-post-container">
        <div class="insight-post">
            <div class="insight-post-image">
                <img src="<?php echo base_url("assets/img/insight_post/".$insight['thumbnail']); ?>?rand=<?php echo $this->config->item('resource_version'); ?>" />
            </div>
            <div class="insight-post-content">
                <div class="category"><?php echo $insight['category_name']; ?></div>
                <div class="title"><?php echo $insight['title']; ?></div>
                <div class="short-content"><?php echo $insight['short_content']; ?></div>
            </div>  
        </div>
    </div>
    </a>

<?php $this->load->view('templates/footer_view'); ?>
<script>
    var start = 10;
    var limit = 9;
    jQuery("#seemore").click(function(e){
        run_seemore();
        e.preventDefault();
    });

    jQuery('#search').keypress(function(e) {
        if(e.which == 10 || e.which == 13) {
            run_search();
            e.preventDefault();
        }
    });

    function run_search(){
        jQuery(".insight-posts").html("");
        start = 0;
        
        search_insights();
    }

    function run_seemore(){
        search_insights();
    }

    function search_insights(){
        var content = jQuery("#hidden_post");
        content.removeClass("hidden");
        var keyword = jQuery("#search").val();
        jQuery.get(base_url + "/insight/get_insights?keyword=" + keyword + "&start=" + start + "&limit=" + limit ,function( data ) {
            var result = jQuery.parseJSON(data);
            var size = result.length;

            if(size > 0){
                for(var a = 0; a < size; a++){
                    var json_data = result[a];
                    content.find(".insight-post-image").find("img").attr("src", base_url + "assets/img/insight_post/" + json_data.thumbnail);
                    content.find(".insight-post-content").find(".category").html(json_data.category_name);
                    content.find(".insight-post-content").find("title").html(json_data.title);
                    content.find(".insight-post-content").find("short-content").html(json_data.short_content);
                    jQuery(".insight-posts").append(content.html());
                }
                jQuery("#seemore").fadeIn();
            } else {
                jQuery("#seemore").fadeOut();
            }

            start+= limit;
        });

        content.addClass("hidden");
    }
</script>