<?php $this->load->view('templates/header_insight_view'); ?>
<div class="insight-detail-header">
    <div class="image-container"><img src="<?php echo base_url("assets/img/insight_post/".$insight_detail['photo']); ?>?rand=<?php echo $this->config->item('resource_version'); ?>"></div>
    <div class="category"><?php echo $insight_detail['category_name']; ?></div>
    <div class="title"><h1><?php echo $insight_detail['title']; ?></h1></div>
    <div class="long-content"><?php echo $insight_detail['long_content']; ?></div>
    
    <div class="social-buttons">
        <div class="social-share-text">
            Share this post
        </div>
        <div class="social-box">
            <a target="_blank" class="not_visible_link" href="https://www.facebook.com/sharer/sharer.php?app_id=<?php echo FACEBOOK_APP_ID; ?>&u=<?php echo site_url("insight/detail")."/".$insight_detail['slug']; ?>&display=popup&ref=plugin&src=share_button">
                <div class="social-media"><img src="<?php echo base_url("assets/img/insight/facebook.png"); ?>?rand=<?php echo $this->config->item('resource_version'); ?>"></div>
            </a>    
            <a target="_blank" class="not_visible_link"  href="
                https://twitter.com/home?status=<?php echo urlencode($insight_detail['title']); ?> <?php echo " ". urlencode(TWEET_MENTION_HASHTAG); ?>">
                <div class="social-media"><img src="<?php echo base_url("assets/img/insight/twitter.png"); ?>?rand=<?php echo $this->config->item('resource_version'); ?>"></div>
            </a>
            <a target="_blank" class="not_visible_link"  href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo site_url("insight/detail")."/".$insight_detail['slug']; ?>&title=<?php echo urlencode($insight_detail['title']); ?>&summary=<?php echo urlencode($insight_detail['short_content']); ?>&source=<?php echo site_url("insight/detail")."/".$insight_detail['slug']; ?>">
                <div class="social-media"><img src="<?php echo base_url("assets/img/insight/linkedin.png"); ?>?rand=<?php echo $this->config->item('resource_version'); ?>"></div>
            </a>  
            <a target="_blank" class="not_visible_link"  href="https://plus.google.com/share?url=<?php echo site_url("insight/detail")."/".$insight_detail['slug']; ?>">  
                <div class="social-media"><img src="<?php echo base_url("assets/img/insight/googleplus.png"); ?>?rand=<?php echo $this->config->item('resource_version'); ?>"></div>
            </a>
            <a target="_blank" class="not_visible_link" href="whatsapp://send?text=<?php echo urlencode($insight_detail['title']); ?> <?php echo site_url("insight/detail")."/".$insight_detail['slug']; ?>" data-action="share/whatsapp/share">
                <div class="social-media"><img src="<?php echo base_url("assets/img/insight/whatsapp.png"); ?>?rand=<?php echo $this->config->item('resource_version'); ?>"></div>
            </a>
        </div>
    </div>
    <div class="other-insights"></div>
</div>
<?php $this->load->view('templates/footer_view'); ?>