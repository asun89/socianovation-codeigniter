<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PrivacyPolicy extends CI_Controller {

	public function index()
	{
		$data = [
			"meta_description" => "A Creative Agency of Antero Makmur located in Jakarta. We handle graphic design, campaign, branding, company profile.",
			"meta_keywords" => "graphic design jakarta, graphic design company, desain grafis, desain grafis jakarta, creative agency jakarta, design logo, branding, company profile, design coorporate, art, advertising, design calendar, desain kalender, annual report, kreatif, agency",
			"meta_title" => "Amco Design - AMCORE Privacy Policy"
		];

		$this->load->view('privacy_policy/index_view', $data);
	}
}
