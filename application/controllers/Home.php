<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$data = [
			"meta_description" => "A Creative Agency of Antero Makmur located in Jakarta. We handle graphic design, campaign, branding, company profile.",
			"meta_keywords" => "graphic design jakarta, graphic design company, desain grafis, desain grafis jakarta, creative agency jakarta, design logo, branding, company profile, design coorporate, art, advertising, design calendar, desain kalender, annual report, kreatif, agency",
			"meta_title" => "Amco Design - Graphic Design Jakarta, Creative Agency & Branding Consultant · Logo · Calendar · Company Profile"
		];

		$data = array_merge($data, $this->initialize_data_from_db());

		$this->load->view('home/index_view', $data);
	}

	private function initialize_data_from_db()
	{
		$company = $this->db->query("SELECT * FROM company")->result_array();

		$histories = $this->db->query("SELECT * FROM histories")->result_array();

		$portfolios = $this->db->query("SELECT * FROM portfolios")->result_array();

		$services = $this->db->query("SELECT * FROM services")->result_array();

		$teams = $this->db->query("SELECT * FROM teams")->result_array();

		$data = array(
			'company' => $company, 
			'histories' => $histories, 
			'portfolios' => $portfolios,
			'services' => $services,
			'teams' => $teams,
			);

		return $data;
	}
}
