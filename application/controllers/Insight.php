<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Insight extends CI_Controller {

	public function index()
	{
		$this->load->model('insight_model');

		$insights_featured = $this->insight_model->get_insights_featured();
		$insights_not_featured = $this->insight_model->get_insights_not_featured();	

		$insight_featured_top = array();
		$insight_featured_left = array();
		$insight_featured_right = array();

		$insight_counter = 0;
		if(isset($insights_featured[0])){
			$insight_featured_top = $insights_featured[0];
		} else{
			if(count($insights_not_featured) > $insight_counter){
				$insight_featured_top = $insights_not_featured[$insight_counter];
				$insight_counter++;
			}
		}
		if(isset($insights_featured[1])){
			$insight_featured_left = $insights_featured[1];
		} else {
			if(count($insights_not_featured) > $insight_counter){
				$insight_featured_left = $insights_not_featured[$insight_counter];
				$insight_counter++;
			}
		}
		if(isset($insights_featured[2])){
			$insight_featured_right = $insights_featured[2];
		} else {
			if(count($insights_not_featured) > $insight_counter){
				$insight_featured_right = $insights_not_featured[$insight_counter];
				$insight_counter++;
			}
		}
		
		$insight_not_featured_will_show = array();
		$counter = 0;
		foreach($insights_not_featured as $insight){
			if($counter >= $insight_counter){
				$insight_not_featured_will_show[] = $insight;
			}
			$counter++;
		}
		$data = array(
			"insight_featured_top" => $insight_featured_top,
			"insight_featured_left" => $insight_featured_left,
			"insight_featured_right" => $insight_featured_right,
			"insights_not_featured" => $insight_not_featured_will_show,
			"meta_description" => "A Creative Agency of Antero Makmur located in Jakarta. We handle graphic design, campaign, branding, company profile.",
			"meta_keywords" => "graphic design jakarta, graphic design company, desain grafis, desain grafis jakarta, creative agency jakarta, design logo, branding, company profile, design coorporate, art, advertising, design calendar, desain kalender, annual report, kreatif, agency",
			"meta_title" => "Amco Design - Graphic Design Jakarta, Creative Agency & Branding Consultant · Logo · Calendar · Company Profile"
		);

		$this->load->view('insight/index_view', $data);
	}

	public function detail($slug){
		$this->load->model('insight_model');
		$insight_detail = $this->insight_model->get_insight_detail_by_slug($slug);

		$data = array(
			"insight_detail" => $insight_detail,
			"meta_description" => "Amco Insight - ".$insight_detail['short_content'],
			"meta_keywords" => $insight_detail['keywords'],
			"meta_title" => "Amco Insight - ".$insight_detail['title']
		);
		$this->load->view('insight/detail_view', $data);
	}

	public function get_insights(){
		$this->load->model("insight_model");
		$keyword = $this->input->get("keyword");

		$start = 10;
		if($this->input->get("start") != null){
			$start = $this->input->get("start");
		}
		$limit = 9;
		if($this->input->get("limit") != null){
			$limit = $this->input->get("limit");
		}
		
		$insights = [];
		if($keyword != "" && $keyword != " " && $keyword != "  "){
			$insights = $this->insight_model->get_insights_by_keyword_paginated($keyword, $start, $limit);
		} else {
			$insights = $this->insight_model->get_insights_by_keyword_paginated("", $start, $limit);
		}

		echo json_encode($insights);
	}
}
