<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Insight_model extends CI_model {

	public function get_insights_featured(){
        $sql = "SELECT 
        i.id, title, c.name as category_name, thumbnail_featured, thumbnail_featured_mini, short_content, slug, is_featured, precedence 
        FROM insights i
        JOIN insight_categories c 
        ON i.category = c.id
        WHERE is_featured = 1 ORDER BY precedence ASC LIMIT 9";
        return $this->db->query($sql)->result_array();
    }

    public function get_insights_not_featured(){
        $sql = "SELECT 
        i.id, title, c.name as category_name, thumbnail, short_content, slug, is_featured, precedence 
        FROM insights i
        JOIN insight_categories c 
        ON i.category = c.id
        WHERE is_featured = 0 
        ORDER BY precedence ASC LIMIT 9";
        return $this->db->query($sql)->result_array();
    }

    public function get_insight_detail_by_slug($slug){
        $sql = "SELECT 
        i.id, title, c.name as category_name, photo, 
        long_content, slug, is_featured, precedence,
        keywords, short_content
        FROM insights i
        JOIN insight_categories c
        ON i.category = c.id
        WHERE slug = ? 
        ORDER BY precedence ASC LIMIT 1";
        return $this->db->query($sql, array($slug))->row_array();
    }

    public function get_insights_by_keyword_paginated($keyword, $start, $limit){
        $sql = "SELECT 
        i.id, title, c.name as category_name, thumbnail, short_content, slug, is_featured, precedence 
        FROM insights i
        JOIN insight_categories c 
        ON i.category = c.id
        WHERE is_featured = 0 AND (
            title LIKE '%".$keyword."%'
            OR short_content LIKE '%".$keyword."%'
            OR long_content LIKE '%".$keyword."%'
        )
        ORDER BY precedence ASC LIMIT ".$start.", 9";

        return $this->db->query($sql)->result_array();
    }
}
