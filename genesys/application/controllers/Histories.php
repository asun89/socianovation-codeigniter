<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Histories extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Histories_model');
        $this->load->library('form_validation');

        if(!$this->session->userdata('logined') || $this->session->userdata('logined') != true)
        {
            redirect('/');
        }        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('histories/histories_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Histories_model->json();
    }

    public function read($id) 
    {
        $row = $this->Histories_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'type' => $row->type,
		'date_description' => $row->date_description,
		'title' => $row->title,
		'description' => $row->description,
		'image' => $row->image,
		'created_datetime' => $row->created_datetime,
		'updated_datetime' => $row->updated_datetime,
		'created_by' => $row->created_by,
		'updated_by' => $row->updated_by,
	    );
            $this->load->view('histories/histories_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('histories'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('histories/create_action'),
	    'id' => set_value('id'),
	    'type' => set_value('type'),
	    'date_description' => set_value('date_description'),
	    'title' => set_value('title'),
	    'description' => set_value('description'),
	    'image' => set_value('image'),
	    'created_datetime' => set_value('created_datetime'),
	    'updated_datetime' => set_value('updated_datetime'),
	    'created_by' => set_value('created_by'),
	    'updated_by' => set_value('updated_by'),
	);
        $this->load->view('histories/histories_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'type' => $this->input->post('type',TRUE),
		'date_description' => $this->input->post('date_description',TRUE),
		'title' => $this->input->post('title',TRUE),
		'description' => $this->input->post('description',TRUE),
		'image' => $this->input->post('image',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Histories_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('histories'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Histories_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('histories/update_action'),
		'id' => set_value('id', $row->id),
		'type' => set_value('type', $row->type),
		'date_description' => set_value('date_description', $row->date_description),
		'title' => set_value('title', $row->title),
		'description' => set_value('description', $row->description),
		'image' => set_value('image', $row->image),
		'created_datetime' => set_value('created_datetime', $row->created_datetime),
		'updated_datetime' => set_value('updated_datetime', $row->updated_datetime),
		'created_by' => set_value('created_by', $row->created_by),
		'updated_by' => set_value('updated_by', $row->updated_by),
	    );
            $this->load->view('histories/histories_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('histories'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'type' => $this->input->post('type',TRUE),
		'date_description' => $this->input->post('date_description',TRUE),
		'title' => $this->input->post('title',TRUE),
		'description' => $this->input->post('description',TRUE),
		'image' => $this->input->post('image',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Histories_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('histories'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Histories_model->get_by_id($id);

        if ($row) {
            $this->Histories_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('histories'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('histories'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('type', 'type', 'trim|required');
	$this->form_validation->set_rules('date_description', 'date description', 'trim|required');
	$this->form_validation->set_rules('title', 'title', 'trim|required');
	$this->form_validation->set_rules('description', 'description', 'trim|required');
	$this->form_validation->set_rules('image', 'image', 'trim|required');
	$this->form_validation->set_rules('created_datetime', 'created datetime', 'trim|required');
	$this->form_validation->set_rules('updated_datetime', 'updated datetime', 'trim|required');
	$this->form_validation->set_rules('created_by', 'created by', 'trim|required');
	$this->form_validation->set_rules('updated_by', 'updated by', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "histories.xls";
        $judul = "histories";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Type");
	xlsWriteLabel($tablehead, $kolomhead++, "Date Description");
	xlsWriteLabel($tablehead, $kolomhead++, "Title");
	xlsWriteLabel($tablehead, $kolomhead++, "Description");
	xlsWriteLabel($tablehead, $kolomhead++, "Image");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Created By");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated By");

	foreach ($this->Histories_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->type);
	    xlsWriteLabel($tablebody, $kolombody++, $data->date_description);
	    xlsWriteLabel($tablebody, $kolombody++, $data->title);
	    xlsWriteLabel($tablebody, $kolombody++, $data->description);
	    xlsWriteLabel($tablebody, $kolombody++, $data->image);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_datetime);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_datetime);
	    xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
	    xlsWriteNumber($tablebody, $kolombody++, $data->updated_by);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=histories.doc");

        $data = array(
            'histories_data' => $this->Histories_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('histories/histories_doc',$data);
    }

}

/* End of file Histories.php */
/* Location: ./application/controllers/Histories.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-05-17 10:14:53 */
/* http://harviacode.com */