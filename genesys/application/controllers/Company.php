<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Company extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Company_model');
        $this->load->library('form_validation');

        if(!$this->session->userdata('logined') || $this->session->userdata('logined') != true)
        {
            redirect('/');
        }        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('company/company_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Company_model->json();
    }

    public function read($id) 
    {
        $row = $this->Company_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'twitter_link' => $row->twitter_link,
		'facebook_link' => $row->facebook_link,
		'github_link' => $row->github_link,
		'year' => $row->year,
		'created_datetime' => $row->created_datetime,
		'updated_datetime' => $row->updated_datetime,
		'created_by' => $row->created_by,
		'updated_by' => $row->updated_by,
	    );
            $this->load->view('company/company_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('company'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('company/create_action'),
	    'id' => set_value('id'),
	    'twitter_link' => set_value('twitter_link'),
	    'facebook_link' => set_value('facebook_link'),
	    'github_link' => set_value('github_link'),
	    'year' => set_value('year'),
	    'created_datetime' => set_value('created_datetime'),
	    'updated_datetime' => set_value('updated_datetime'),
	    'created_by' => set_value('created_by'),
	    'updated_by' => set_value('updated_by'),
	);
        $this->load->view('company/company_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'twitter_link' => $this->input->post('twitter_link',TRUE),
		'facebook_link' => $this->input->post('facebook_link',TRUE),
		'github_link' => $this->input->post('github_link',TRUE),
		'year' => $this->input->post('year',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Company_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('company'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Company_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('company/update_action'),
		'id' => set_value('id', $row->id),
		'twitter_link' => set_value('twitter_link', $row->twitter_link),
		'facebook_link' => set_value('facebook_link', $row->facebook_link),
		'github_link' => set_value('github_link', $row->github_link),
		'year' => set_value('year', $row->year),
		'created_datetime' => set_value('created_datetime', $row->created_datetime),
		'updated_datetime' => set_value('updated_datetime', $row->updated_datetime),
		'created_by' => set_value('created_by', $row->created_by),
		'updated_by' => set_value('updated_by', $row->updated_by),
	    );
            $this->load->view('company/company_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('company'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'twitter_link' => $this->input->post('twitter_link',TRUE),
		'facebook_link' => $this->input->post('facebook_link',TRUE),
		'github_link' => $this->input->post('github_link',TRUE),
		'year' => $this->input->post('year',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Company_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('company'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Company_model->get_by_id($id);

        if ($row) {
            $this->Company_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('company'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('company'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('twitter_link', 'twitter link', 'trim|required');
	$this->form_validation->set_rules('facebook_link', 'facebook link', 'trim|required');
	$this->form_validation->set_rules('github_link', 'github link', 'trim|required');
	$this->form_validation->set_rules('year', 'year', 'trim|required');
	$this->form_validation->set_rules('created_datetime', 'created datetime', 'trim|required');
	$this->form_validation->set_rules('updated_datetime', 'updated datetime', 'trim|required');
	$this->form_validation->set_rules('created_by', 'created by', 'trim|required');
	$this->form_validation->set_rules('updated_by', 'updated by', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "company.xls";
        $judul = "company";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Twitter Link");
	xlsWriteLabel($tablehead, $kolomhead++, "Facebook Link");
	xlsWriteLabel($tablehead, $kolomhead++, "Github Link");
	xlsWriteLabel($tablehead, $kolomhead++, "Year");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Created By");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated By");

	foreach ($this->Company_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->twitter_link);
	    xlsWriteLabel($tablebody, $kolombody++, $data->facebook_link);
	    xlsWriteLabel($tablebody, $kolombody++, $data->github_link);
	    xlsWriteLabel($tablebody, $kolombody++, $data->year);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_datetime);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_datetime);
	    xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
	    xlsWriteNumber($tablebody, $kolombody++, $data->updated_by);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=company.doc");

        $data = array(
            'company_data' => $this->Company_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('company/company_doc',$data);
    }

}

/* End of file Company.php */
/* Location: ./application/controllers/Company.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-05-17 10:14:53 */
/* http://harviacode.com */