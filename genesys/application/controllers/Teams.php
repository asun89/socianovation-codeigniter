<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Teams extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Teams_model');
        $this->load->library('form_validation');

        if(!$this->session->userdata('logined') || $this->session->userdata('logined') != true)
        {
            redirect('/');
        }        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('teams/teams_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Teams_model->json();
    }

    public function read($id) 
    {
        $row = $this->Teams_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'name' => $row->name,
		'photo' => $row->photo,
		'structure_title' => $row->structure_title,
		'job_title' => $row->job_title,
		'twitter' => $row->twitter,
		'facebook' => $row->facebook,
		'linkedin' => $row->linkedin,
		'created_datetime' => $row->created_datetime,
		'updated_datetime' => $row->updated_datetime,
		'created_by' => $row->created_by,
		'updated_by' => $row->updated_by,
	    );
            $this->load->view('teams/teams_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('teams'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('teams/create_action'),
	    'id' => set_value('id'),
	    'name' => set_value('name'),
	    'photo' => set_value('photo'),
	    'structure_title' => set_value('structure_title'),
	    'job_title' => set_value('job_title'),
	    'twitter' => set_value('twitter'),
	    'facebook' => set_value('facebook'),
	    'linkedin' => set_value('linkedin'),
	    'created_datetime' => set_value('created_datetime'),
	    'updated_datetime' => set_value('updated_datetime'),
	    'created_by' => set_value('created_by'),
	    'updated_by' => set_value('updated_by'),
	);
        $this->load->view('teams/teams_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'photo' => $this->input->post('photo',TRUE),
		'structure_title' => $this->input->post('structure_title',TRUE),
		'job_title' => $this->input->post('job_title',TRUE),
		'twitter' => $this->input->post('twitter',TRUE),
		'facebook' => $this->input->post('facebook',TRUE),
		'linkedin' => $this->input->post('linkedin',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Teams_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('teams'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Teams_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('teams/update_action'),
		'id' => set_value('id', $row->id),
		'name' => set_value('name', $row->name),
		'photo' => set_value('photo', $row->photo),
		'structure_title' => set_value('structure_title', $row->structure_title),
		'job_title' => set_value('job_title', $row->job_title),
		'twitter' => set_value('twitter', $row->twitter),
		'facebook' => set_value('facebook', $row->facebook),
		'linkedin' => set_value('linkedin', $row->linkedin),
		'created_datetime' => set_value('created_datetime', $row->created_datetime),
		'updated_datetime' => set_value('updated_datetime', $row->updated_datetime),
		'created_by' => set_value('created_by', $row->created_by),
		'updated_by' => set_value('updated_by', $row->updated_by),
	    );
            $this->load->view('teams/teams_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('teams'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'photo' => $this->input->post('photo',TRUE),
		'structure_title' => $this->input->post('structure_title',TRUE),
		'job_title' => $this->input->post('job_title',TRUE),
		'twitter' => $this->input->post('twitter',TRUE),
		'facebook' => $this->input->post('facebook',TRUE),
		'linkedin' => $this->input->post('linkedin',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Teams_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('teams'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Teams_model->get_by_id($id);

        if ($row) {
            $this->Teams_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('teams'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('teams'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('photo', 'photo', 'trim|required');
	$this->form_validation->set_rules('structure_title', 'structure title', 'trim|required');
	$this->form_validation->set_rules('job_title', 'job title', 'trim|required');
	$this->form_validation->set_rules('twitter', 'twitter', 'trim|required');
	$this->form_validation->set_rules('facebook', 'facebook', 'trim|required');
	$this->form_validation->set_rules('linkedin', 'linkedin', 'trim|required');
	$this->form_validation->set_rules('created_datetime', 'created datetime', 'trim|required');
	$this->form_validation->set_rules('updated_datetime', 'updated datetime', 'trim|required');
	$this->form_validation->set_rules('created_by', 'created by', 'trim|required');
	$this->form_validation->set_rules('updated_by', 'updated by', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "teams.xls";
        $judul = "teams";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Name");
	xlsWriteLabel($tablehead, $kolomhead++, "Photo");
	xlsWriteLabel($tablehead, $kolomhead++, "Structure Title");
	xlsWriteLabel($tablehead, $kolomhead++, "Job Title");
	xlsWriteLabel($tablehead, $kolomhead++, "Twitter");
	xlsWriteLabel($tablehead, $kolomhead++, "Facebook");
	xlsWriteLabel($tablehead, $kolomhead++, "Linkedin");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Created By");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated By");

	foreach ($this->Teams_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->name);
	    xlsWriteLabel($tablebody, $kolombody++, $data->photo);
	    xlsWriteLabel($tablebody, $kolombody++, $data->structure_title);
	    xlsWriteLabel($tablebody, $kolombody++, $data->job_title);
	    xlsWriteLabel($tablebody, $kolombody++, $data->twitter);
	    xlsWriteLabel($tablebody, $kolombody++, $data->facebook);
	    xlsWriteLabel($tablebody, $kolombody++, $data->linkedin);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_datetime);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_datetime);
	    xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
	    xlsWriteNumber($tablebody, $kolombody++, $data->updated_by);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=teams.doc");

        $data = array(
            'teams_data' => $this->Teams_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('teams/teams_doc',$data);
    }

}

/* End of file Teams.php */
/* Location: ./application/controllers/Teams.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-05-17 10:14:53 */
/* http://harviacode.com */