<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Portfolios extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Portfolios_model');
        $this->load->library('form_validation');

        if(!$this->session->userdata('logined') || $this->session->userdata('logined') != true)
        {
            redirect('/');
        }        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('portfolios/portfolios_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Portfolios_model->json();
    }

    public function read($id) 
    {
        $row = $this->Portfolios_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'name' => $row->name,
		'link' => $row->link,
		'link_title' => $row->link_title,
		'thumbnail' => $row->thumbnail,
		'image' => $row->image,
		'description' => $row->description,
		'client' => $row->client,
		'date' => $row->date,
		'service' => $row->service,
		'created_datetime' => $row->created_datetime,
		'updated_datetime' => $row->updated_datetime,
		'created_by' => $row->created_by,
		'updated_by' => $row->updated_by,
	    );
            $this->load->view('portfolios/portfolios_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('portfolios'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('portfolios/create_action'),
	    'id' => set_value('id'),
	    'name' => set_value('name'),
	    'link' => set_value('link'),
	    'link_title' => set_value('link_title'),
	    'thumbnail' => set_value('thumbnail'),
	    'image' => set_value('image'),
	    'description' => set_value('description'),
	    'client' => set_value('client'),
	    'date' => set_value('date'),
	    'service' => set_value('service'),
	    'created_datetime' => set_value('created_datetime'),
	    'updated_datetime' => set_value('updated_datetime'),
	    'created_by' => set_value('created_by'),
	    'updated_by' => set_value('updated_by'),
	);
        $this->load->view('portfolios/portfolios_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            /* Upload File */
            $this->load->library('upload');
            $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);

            $nmfile = "file_".time().".".$ext; //nama file + fungsi time
            $config['upload_path'] = ASSET_PATH.'portfolio/'; //Folder untuk menyimpan hasil upload
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
            $config['max_size'] = '20000'; //maksimum besar file 20M
            $config['max_width']  = '10000'; //lebar maksimum 10000 px
            $config['max_height']  = '10000'; //tinggi maksimu 10000 px
            $config['file_name'] = $nmfile; //nama yang terupload nantinya

            $this->upload->initialize($config);

            if($_FILES['image']['name'])
            {
                if ($this->upload->do_upload('image'))
                {
                    $config2['image_library'] = 'gd2'; 
                    $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                    $config2['new_image'] = $this->upload->upload_path.'thumb_'.$this->upload->file_name; 
                    $config2['maintain_ratio'] = TRUE;
                    $config2['width'] = 400; //lebar setelah resize menjadi 100 px
                    $config2['height'] = 260; //lebar setelah resize menjadi 100 px
                    $this->load->library('image_lib',$config2); 

                    //pesan yang muncul jika resize error dimasukkan pada session flashdata
                    if ( !$this->image_lib->resize())
                    {
                        $this->session->set_flashdata('errors', $this->image_lib->display_errors('', '')); 

                    }

                    $data['name'] = $this->input->post('name',TRUE);
                    $data['link'] = $this->input->post('link',TRUE);
                    $data['link_title'] = $this->input->post('link_title',TRUE);
                    $data['description'] = $this->input->post('description',TRUE);
                    $data['client'] = $this->input->post('client',TRUE);
                    $data['date'] = $this->input->post('date',TRUE);
                    $data['service'] = $this->input->post('service',TRUE);
                    $data['image'] = $this->upload->file_name;
                    $data['thumbnail'] = 'thumb_'.$this->upload->file_name;
                    $data['created_datetime'] = date('Y-m-d H:i:s');
                    $data['updated_datetime'] = date('Y-m-d H:i:s');
                    $data['created_by'] = 1;
                    $data['updated_by'] = 1;

                    $this->Portfolios_model->insert($data);
                    $this->session->set_flashdata('message', 'Create Record Success');
                    redirect(site_url('portfolios'));

                }else{
                    $this->session->set_flashdata('message', 'Create Record Failed (Image not Uploaded)');
                    redirect(site_url('portfolios'));
                }
            }
            /* Upload File */
        }
    }
    
    public function update($id) 
    {
        $row = $this->Portfolios_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('portfolios/update_action'),
		'id' => set_value('id', $row->id),
		'name' => set_value('name', $row->name),
		'link' => set_value('link', $row->link),
		'link_title' => set_value('link_title', $row->link_title),
		'thumbnail' => set_value('thumbnail', $row->thumbnail),
		'image' => set_value('image', $row->image),
        'old_image' => set_value('image', $row->image),
		'description' => set_value('description', $row->description),
		'client' => set_value('client', $row->client),
		'date' => set_value('date', $row->date),
		'service' => set_value('service', $row->service),
		'created_datetime' => set_value('created_datetime', $row->created_datetime),
		'updated_datetime' => set_value('updated_datetime', $row->updated_datetime),
		'created_by' => set_value('created_by', $row->created_by),
		'updated_by' => set_value('updated_by', $row->updated_by),
	    );
            $this->load->view('portfolios/portfolios_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('portfolios'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {

            /* Upload File */
            if($_FILES['image']['name'] != "")
            {
                $this->load->library('upload');
                $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);

                $nmfile = "file_".time().".".$ext; //nama file + fungsi time
                $config['upload_path'] = ASSET_PATH.'portfolio/'; //Folder untuk menyimpan hasil upload
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
                $config['max_size'] = '20000'; //maksimum besar file 20M
                $config['max_width']  = '10000'; //lebar maksimum 10000 px
                $config['max_height']  = '10000'; //tinggi maksimu 10000 px
                $config['file_name'] = $nmfile; //nama yang terupload nantinya

                $this->upload->initialize($config);

                if($_FILES['image']['name'])
                {
                    if ($this->upload->do_upload('image'))
                    {
                        $config2['image_library'] = 'gd2'; 
                        $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config2['new_image'] = $this->upload->upload_path.'thumb_'.$this->upload->file_name; 
                        $config2['maintain_ratio'] = TRUE;
                        $config2['width'] = 400; //lebar setelah resize menjadi 100 px
                        $config2['height'] = 260; //lebar setelah resize menjadi 100 px
                        $this->load->library('image_lib',$config2); 

                        //pesan yang muncul jika resize error dimasukkan pada session flashdata
                        if ( !$this->image_lib->resize())
                        {
                            $this->session->set_flashdata('errors', $this->image_lib->display_errors()); 

                        }

                        $data['name'] = $this->input->post('name',TRUE);
                        $data['link'] = $this->input->post('link',TRUE);
                        $data['link_title'] = $this->input->post('link_title',TRUE);
                        $data['description'] = $this->input->post('description',TRUE);
                        $data['client'] = $this->input->post('client',TRUE);
                        $data['date'] = $this->input->post('date',TRUE);
                        $data['service'] = $this->input->post('service',TRUE);
                        $data['image'] = $this->upload->file_name;
                        $data['thumbnail'] = 'thumb_'.$this->upload->file_name;
                        $data['updated_datetime'] = date('Y-m-d H:i:s');
                        $data['updated_by'] = 1;

                        /* Delete old File */
                        unlink(ASSET_PATH.'portfolio/'.$this->input->post('old_image'));
                        unlink(ASSET_PATH.'portfolio/thumb_'.$this->input->post('old_image'));
                        /* Delete old File */

                        $this->Portfolios_model->update($this->input->post('id', TRUE), $data);
                        $this->session->set_flashdata('message', 'Update Record Success');
                        redirect(site_url('portfolios'));

                    }else{
                        $this->session->set_flashdata('message', 'Create Record Failed (Image not Uploaded)');
                        redirect(site_url('portfolios'));
                    }
                }
            }
            else
            {
                /* Upload File */
                $data['name'] = $this->input->post('name',TRUE);
                $data['link'] = $this->input->post('link',TRUE);
                $data['link_title'] = $this->input->post('link_title',TRUE);
                $data['description'] = $this->input->post('description',TRUE);
                $data['client'] = $this->input->post('client',TRUE);
                $data['date'] = $this->input->post('date',TRUE);
                $data['service'] = $this->input->post('service',TRUE);
                $data['image'] = $this->input->post('old_image',TRUE);
                $data['updated_datetime'] = date('Y-m-d H:i:s');
                $data['updated_by'] = 1;

                $this->Portfolios_model->update($this->input->post('id', TRUE), $data);
                $this->session->set_flashdata('message', 'Update Record Success');
                redirect(site_url('portfolios'));
            }
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Portfolios_model->get_by_id($id);

        if ($row) {
            $this->Portfolios_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('portfolios'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('portfolios'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('link', 'link', 'trim|required');
	$this->form_validation->set_rules('link_title', 'link title', 'trim|required');
    $this->form_validation->set_rules('image', 'File', 'trim|xss_clean');
	$this->form_validation->set_rules('description', 'description', 'trim|required');
	$this->form_validation->set_rules('client', 'client', 'trim|required');
	$this->form_validation->set_rules('date', 'date', 'trim|required');
	$this->form_validation->set_rules('service', 'service', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "portfolios.xls";
        $judul = "portfolios";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Name");
	xlsWriteLabel($tablehead, $kolomhead++, "Link");
	xlsWriteLabel($tablehead, $kolomhead++, "Link Title");
	xlsWriteLabel($tablehead, $kolomhead++, "Thumbnail");
	xlsWriteLabel($tablehead, $kolomhead++, "Image");
	xlsWriteLabel($tablehead, $kolomhead++, "Description");
	xlsWriteLabel($tablehead, $kolomhead++, "Client");
	xlsWriteLabel($tablehead, $kolomhead++, "Date");
	xlsWriteLabel($tablehead, $kolomhead++, "Service");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Created By");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated By");

	foreach ($this->Portfolios_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->name);
	    xlsWriteLabel($tablebody, $kolombody++, $data->link);
	    xlsWriteLabel($tablebody, $kolombody++, $data->link_title);
	    xlsWriteLabel($tablebody, $kolombody++, $data->thumbnail);
	    xlsWriteLabel($tablebody, $kolombody++, $data->image);
	    xlsWriteLabel($tablebody, $kolombody++, $data->description);
	    xlsWriteLabel($tablebody, $kolombody++, $data->client);
	    xlsWriteLabel($tablebody, $kolombody++, $data->date);
	    xlsWriteLabel($tablebody, $kolombody++, $data->service);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_datetime);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_datetime);
	    xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
	    xlsWriteNumber($tablebody, $kolombody++, $data->updated_by);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=portfolios.doc");

        $data = array(
            'portfolios_data' => $this->Portfolios_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('portfolios/portfolios_doc',$data);
    }

}

/* End of file Portfolios.php */
/* Location: ./application/controllers/Portfolios.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-05-17 10:14:53 */
/* http://harviacode.com */
