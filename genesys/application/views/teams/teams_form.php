<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Teams <?php echo $button ?></h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Name <?php echo form_error('name') ?></label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Photo <?php echo form_error('photo') ?></label>
            <input type="text" class="form-control" name="photo" id="photo" placeholder="Photo" value="<?php echo $photo; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Structure Title <?php echo form_error('structure_title') ?></label>
            <input type="text" class="form-control" name="structure_title" id="structure_title" placeholder="Structure Title" value="<?php echo $structure_title; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Job Title <?php echo form_error('job_title') ?></label>
            <input type="text" class="form-control" name="job_title" id="job_title" placeholder="Job Title" value="<?php echo $job_title; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Twitter <?php echo form_error('twitter') ?></label>
            <input type="text" class="form-control" name="twitter" id="twitter" placeholder="Twitter" value="<?php echo $twitter; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Facebook <?php echo form_error('facebook') ?></label>
            <input type="text" class="form-control" name="facebook" id="facebook" placeholder="Facebook" value="<?php echo $facebook; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Linkedin <?php echo form_error('linkedin') ?></label>
            <input type="text" class="form-control" name="linkedin" id="linkedin" placeholder="Linkedin" value="<?php echo $linkedin; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Created Datetime <?php echo form_error('created_datetime') ?></label>
            <input type="text" class="form-control" name="created_datetime" id="created_datetime" placeholder="Created Datetime" value="<?php echo $created_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Updated Datetime <?php echo form_error('updated_datetime') ?></label>
            <input type="text" class="form-control" name="updated_datetime" id="updated_datetime" placeholder="Updated Datetime" value="<?php echo $updated_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Created By <?php echo form_error('created_by') ?></label>
            <input type="text" class="form-control" name="created_by" id="created_by" placeholder="Created By" value="<?php echo $created_by; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Updated By <?php echo form_error('updated_by') ?></label>
            <input type="text" class="form-control" name="updated_by" id="updated_by" placeholder="Updated By" value="<?php echo $updated_by; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('teams') ?>" class="btn btn-default">Cancel</a>
	</form><?php $this->load->view('templates/footer');?>