<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Teams Read</h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <table class="table">
	    <tr><td>Name</td><td><?php echo $name; ?></td></tr>
	    <tr><td>Photo</td><td><?php echo $photo; ?></td></tr>
	    <tr><td>Structure Title</td><td><?php echo $structure_title; ?></td></tr>
	    <tr><td>Job Title</td><td><?php echo $job_title; ?></td></tr>
	    <tr><td>Twitter</td><td><?php echo $twitter; ?></td></tr>
	    <tr><td>Facebook</td><td><?php echo $facebook; ?></td></tr>
	    <tr><td>Linkedin</td><td><?php echo $linkedin; ?></td></tr>
	    <tr><td>Created Datetime</td><td><?php echo $created_datetime; ?></td></tr>
	    <tr><td>Updated Datetime</td><td><?php echo $updated_datetime; ?></td></tr>
	    <tr><td>Created By</td><td><?php echo $created_by; ?></td></tr>
	    <tr><td>Updated By</td><td><?php echo $updated_by; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('teams') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table><?php $this->load->view('templates/footer');?>