<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Portfolios List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Name</th>
		<th>Link</th>
		<th>Link Title</th>
		<th>Thumbnail</th>
		<th>Image</th>
		<th>Description</th>
		<th>Client</th>
		<th>Date</th>
		<th>Service</th>
		<th>Created Datetime</th>
		<th>Updated Datetime</th>
		<th>Created By</th>
		<th>Updated By</th>
		
            </tr><?php
            foreach ($portfolios_data as $portfolios)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $portfolios->name ?></td>
		      <td><?php echo $portfolios->link ?></td>
		      <td><?php echo $portfolios->link_title ?></td>
		      <td><?php echo $portfolios->thumbnail ?></td>
		      <td><?php echo $portfolios->image ?></td>
		      <td><?php echo $portfolios->description ?></td>
		      <td><?php echo $portfolios->client ?></td>
		      <td><?php echo $portfolios->date ?></td>
		      <td><?php echo $portfolios->service ?></td>
		      <td><?php echo $portfolios->created_datetime ?></td>
		      <td><?php echo $portfolios->updated_datetime ?></td>
		      <td><?php echo $portfolios->created_by ?></td>
		      <td><?php echo $portfolios->updated_by ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>