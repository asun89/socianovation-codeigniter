<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Portfolios <?php echo $button ?></h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	    <div class="form-group">
            <label for="varchar">Name <?php echo form_error('name') ?></label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Link <?php echo form_error('link') ?></label>
            <input type="text" class="form-control" name="link" id="link" placeholder="Link" value="<?php echo $link; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Link Title <?php echo form_error('link_title') ?></label>
            <input type="text" class="form-control" name="link_title" id="link_title" placeholder="Link Title" value="<?php echo $link_title; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Image <?php echo form_error('image') ?></label>
            <input type="file" class="form-control" name="image" id="image" placeholder="Image" value="<?php echo $image; ?>" />

            <?php if(isset($old_image)){ ?>
            <img style="margin-top:10px;" src="<?php echo ASSET_URL; ?>portfolio/<?php echo $old_image; ?>" width="100%">
            <input type="hidden" name="old_image" id="old_image" value="<?php echo $old_image; ?>" />
            <?php } ?>
        </div>
	    <div class="form-group">
            <label for="description">Description <?php echo form_error('description') ?></label>
            <textarea class="form-control" rows="3" name="description" id="description" placeholder="Description"><?php echo $description; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Client <?php echo form_error('client') ?></label>
            <input type="text" class="form-control" name="client" id="client" placeholder="Client" value="<?php echo $client; ?>" />
        </div>
	    <div class="form-group">
            <label for="date">Date <?php echo form_error('date') ?></label>
            <input type="text" class="form-control" name="date" id="date" placeholder="Date" value="<?php echo $date; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Service <?php echo form_error('service') ?></label>
            <input type="text" class="form-control" name="service" id="service" placeholder="Service" value="<?php echo $service; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('portfolios') ?>" class="btn btn-default">Cancel</a>
	</form><?php $this->load->view('templates/footer');?>