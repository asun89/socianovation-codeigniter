<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Company Read</h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <table class="table">
	    <tr><td>Twitter Link</td><td><?php echo $twitter_link; ?></td></tr>
	    <tr><td>Facebook Link</td><td><?php echo $facebook_link; ?></td></tr>
	    <tr><td>Github Link</td><td><?php echo $github_link; ?></td></tr>
	    <tr><td>Year</td><td><?php echo $year; ?></td></tr>
	    <tr><td>Created Datetime</td><td><?php echo $created_datetime; ?></td></tr>
	    <tr><td>Updated Datetime</td><td><?php echo $updated_datetime; ?></td></tr>
	    <tr><td>Created By</td><td><?php echo $created_by; ?></td></tr>
	    <tr><td>Updated By</td><td><?php echo $updated_by; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('company') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table><?php $this->load->view('templates/footer');?>