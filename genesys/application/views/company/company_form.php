<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Company <?php echo $button ?></h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Twitter Link <?php echo form_error('twitter_link') ?></label>
            <input type="text" class="form-control" name="twitter_link" id="twitter_link" placeholder="Twitter Link" value="<?php echo $twitter_link; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Facebook Link <?php echo form_error('facebook_link') ?></label>
            <input type="text" class="form-control" name="facebook_link" id="facebook_link" placeholder="Facebook Link" value="<?php echo $facebook_link; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Github Link <?php echo form_error('github_link') ?></label>
            <input type="text" class="form-control" name="github_link" id="github_link" placeholder="Github Link" value="<?php echo $github_link; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Year <?php echo form_error('year') ?></label>
            <input type="text" class="form-control" name="year" id="year" placeholder="Year" value="<?php echo $year; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Created Datetime <?php echo form_error('created_datetime') ?></label>
            <input type="text" class="form-control" name="created_datetime" id="created_datetime" placeholder="Created Datetime" value="<?php echo $created_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Updated Datetime <?php echo form_error('updated_datetime') ?></label>
            <input type="text" class="form-control" name="updated_datetime" id="updated_datetime" placeholder="Updated Datetime" value="<?php echo $updated_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Created By <?php echo form_error('created_by') ?></label>
            <input type="text" class="form-control" name="created_by" id="created_by" placeholder="Created By" value="<?php echo $created_by; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Updated By <?php echo form_error('updated_by') ?></label>
            <input type="text" class="form-control" name="updated_by" id="updated_by" placeholder="Updated By" value="<?php echo $updated_by; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('company') ?>" class="btn btn-default">Cancel</a>
	</form><?php $this->load->view('templates/footer');?>