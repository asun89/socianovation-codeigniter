<?php  if ( ! defined("BASEPATH")) exit("No direct script access allowed");
	function generate_sidemenu()
	{
		return '<li>
		<a href="'.site_url('admin').'"><i class="fa fa-list fa-fw"></i> Admin</a>
	</li><li>
		<a href="'.site_url('company').'"><i class="fa fa-list fa-fw"></i> Company</a>
	</li><li>
		<a href="'.site_url('histories').'"><i class="fa fa-list fa-fw"></i> Histories</a>
	</li><li>
		<a href="'.site_url('portfolios').'"><i class="fa fa-list fa-fw"></i> Portfolios</a>
	</li><li>
		<a href="'.site_url('services').'"><i class="fa fa-list fa-fw"></i> Services</a>
	</li><li>
		<a href="'.site_url('teams').'"><i class="fa fa-list fa-fw"></i> Teams</a>
	</li>';
	}
